'use client'

import { useState } from 'react'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function Counter () {
  const [count, setCount] = useState(0)
  return (
    <>
      <h2>{count}</h2>
      <button type='button' onClick={() => setCount(count + 1)}>
        +
      </button>
    </>
  )
}

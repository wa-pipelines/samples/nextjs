interface Params {
  params: {
    slug: string
  }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function generateMetadata ({ params }: Params) {
  return { title: `Post: ${params.slug}` }
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function Page ({ params }: Params) {
  return <h1>Slug: {params.slug}</h1>
}
